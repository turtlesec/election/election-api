# Election API
Kotlin api with spring boot.
# CI/CD
Each commit will trigger a pipeline which will package, test, build and the deploy the application if there are no test failures.

# Code-analysis
We use the Sonatype OSS Index to continuously scan our dependencies for known vulnerabilties.
https://sonatype.github.io/ossindex-maven/
# Images
Use only alpine based images to reduce file size and attack surface.

Images are stored in the project's internal gitlab image registry.
The retention policy for the registry is currently set to one week, except for tags matching the regex `.*RELEASE.*`.

# Image scan
We use the GitLab’s Klar analyzer to scan the containers. Klar as a wrapper for Clair.
https://github.com/optiopay/klar
# Deployment
Performed by scp'ing over the docker-compose file to the host and running docker-compose up & down via ssh.
(See scripts/deploy.sh)