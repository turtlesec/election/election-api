package no.election.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = ["entitynr"])])
data class Protocol(
        val entityNr: String,
        val electionYear: String,
        val downloadTime: String?,
        val election: String,
        val municipality: String,
        val county: String,
        val absoluteDeviation: Int,
        val reportedDeviation: Int
) {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    val id = 0
}

