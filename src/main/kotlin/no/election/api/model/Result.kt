package no.election.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import no.election.api.dto.*
import javax.persistence.*

@JsonIgnoreProperties(ignoreUnknown = true)

@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = ["name", "parent"])])
data class Result(
        val parent: String?,
        val parentOriginalName: String?,
        val level: String,
        val name: String,
        val originalName: String,
        val nr: String,
        val year: Int,
        val electionLevel: String,
        val electionTime: ElectionTime,
        val electorates: Int,
        val totalMandates: TotalMandates,
        val votes: Votes,
        val voting: Voting,
        val participation: Participation,
        val count: Count,
        @OneToMany(cascade = [CascadeType.ALL])
        val parties: List<Parties>
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id = 0 //
}
