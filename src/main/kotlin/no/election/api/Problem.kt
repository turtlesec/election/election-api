package no.election.api

class Problem(
        val title: String,
        val status: Int,
        val detail: String,
        val code: String
)