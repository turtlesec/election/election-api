package no.election.api.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import no.election.api.model.Protocol
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import kotlin.math.absoluteValue
import kotlin.reflect.full.memberProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ProtocolDto(
        val electionYear: String,
        val downloadTime: String?,
        val election: String,
        val municipality: String,
        val county: String,
        val numbers: Numbers,
        var entityNr: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Numbers(
        @JsonProperty("D2.4 Avvik mellom foreløpig og endelig opptelling av ordinære valgtingsstemmesedler")
        val deviation: Map<String, DeviationDto>
)

@Entity
data class Deviation(
        val name: String,
        val preliminary: Int,
        val final: Int,
        val deviation: Int
) {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    val id: Int = 0
}

data class DeviationDto(
        @JsonProperty("Foreløpig") val preliminary: Int,
        @JsonProperty("Endelig") val final: Int,
        @JsonProperty("Avvik") val deviation: Int
)


fun DeviationDto.toDeviation(id: String, name: String) = with(::Deviation) {
    val propertiesByName = DeviationDto::class.memberProperties.associateBy { it.name }
    callBy(parameters.associateWith { parameter ->
        when (parameter.name) {
            Deviation::name.name -> name
            else -> propertiesByName[parameter.name]?.get(this@toDeviation)
        }
    })
}

fun ProtocolDto.toProtocol(id: String) = with(::Protocol) {
    val propertiesByName = ProtocolDto::class.memberProperties.associateBy { it.name }
    callBy(parameters.associateWith { parameter ->
        when (parameter.name) {
            Protocol::entityNr.name -> id
            Protocol::absoluteDeviation.name -> numbers.absoluteDeviation()
            Protocol::reportedDeviation.name -> numbers.reportedDeviation()
            else -> propertiesByName[parameter.name]?.get(this@toProtocol)
        }
    })
}

fun Numbers.absoluteDeviation() = this.deviation.map {
    if (it.key != "Totalt antall partifordelte stemmesedler") {
        it.value.deviation.absoluteValue
    } else {
        0
    }
}.sum()

fun Numbers.reportedDeviation() = this.deviation.filter { it.key == "Totalt antall partifordelte stemmesedler" }.toList().first().second.deviation
