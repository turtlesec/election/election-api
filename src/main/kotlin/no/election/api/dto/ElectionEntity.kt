package no.election.api.dto

data class ElectionEntity(
        val name: String,
        val county: String,
        val nr: String,
        val originalName: String,
        val parentOriginalName: String
) {
    val link: String = "/api/results/${nr}"
}