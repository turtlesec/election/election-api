package no.election.api.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import no.election.api.model.Result
import org.apache.commons.lang3.StringUtils
import javax.persistence.*
import kotlin.reflect.full.memberProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ResultDto(
        @JsonProperty("id") val identifier: Identifier,
        @JsonProperty("tidspunkt") val electionTime: ElectionTime,
        @JsonProperty("antallsb") val electorates: Int,
        @JsonProperty("mandater") val totalMandates: TotalMandates,
        @JsonProperty("stemmer") val votes: Votes,
        @JsonProperty("stemmegiving") val voting: Voting,
        @JsonProperty("frammote") val participation: Participation,
        @JsonProperty("opptalt") val count: Count,
        @JsonProperty("partier") val parties: List<Parties>,
        @JsonProperty("_links") val links: Links

)

fun ResultDto.toResult() = with(::Result) {
    val propertiesByName = ResultDto::class.memberProperties.associateBy { it.name }
    callBy(parameters.associateWith { parameter ->
        when (parameter.name) {
            Result::parent.name -> "${links.up.name}".spaceToUnderscore().stripAccents().toLowerCase().norwegianToEnglishCharacters()
            Result::parentOriginalName.name -> "${links.up.name}"
            Result::name.name -> identifier.name.spaceToUnderscore().toLowerCase().norwegianToEnglishCharacters()
            Result::originalName.name -> identifier.name
            Result::level.name -> identifier.level.toLowerCase()
            Result::nr.name -> identifier.nr
            Result::year.name -> identifier.electionYear
            Result::electionLevel.name -> identifier.electionType.toLowerCase()
            else -> propertiesByName[parameter.name]?.get(this@toResult)
        }
    })
}

private fun String.spaceToUnderscore() =
        this.replace(" ", "_")

private fun String.norwegianToEnglishCharacters() =
        this.replace("å", "a")
                .replace("æ", "a")
                .replace("ø", "o")

private fun String.stripAccents() =
        StringUtils.stripAccents(this)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Links(
        @JsonProperty("related") val related: List<Related>,
        @JsonProperty("self") val self: Related,
        @JsonProperty("up") val up: Related
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Related(
        @JsonProperty("nr") val nr: String?,
        @JsonProperty("href") val href: String,
        @JsonProperty("hrefNavn") val hrefName: String,
        @JsonProperty("navn") val name: String?
)

@Embeddable
data class TotalMandates(
        @Column @JsonProperty("antall") val quantity: Int,
        @Column @JsonProperty("endring") val change: Int?
)

@Embeddable
data class ElectionTime(
        @Column @JsonProperty("rapportGenerert") val reportGenerated: String,
        @Column @JsonProperty("sisteStemmer") val lastVotes: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Identifier(
        @JsonProperty("valgaar") val electionYear: Int?,
        @JsonProperty("valgtype") val electionType: String,
        @JsonProperty("nivaa") val level: String,
        @JsonProperty("navn") val name: String,
        @JsonProperty("nr") val nr: String
)

@Entity
data class Parties(
        @OneToOne(cascade = [CascadeType.ALL]) @JsonProperty("id") val partyIdentifier: PartyIdentifier,
        @Column @JsonProperty("stemmer") val partyVotes: PartyVotes,
        @Column @JsonProperty("mandater") val mandates: Mandates?
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val partyId: Int = 0
}

@Entity
@Access(AccessType.FIELD)
data class PartyIdentifier(
        @Column @JsonProperty("partikategori") val category: Int?,
        @Column @JsonProperty("partikode") val code: String,
        @Column @JsonProperty("navn") val name: String
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val partyIdentifierId = 0
}

@Embeddable
@Access(AccessType.FIELD)
data class PartyVotes(
        @JsonProperty("resultat") @Embedded val partyResult: PartyResult
)

@Access(AccessType.FIELD)
@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
data class PartyResult(
        @JsonProperty("prosent") val percent: Double?,
        @JsonProperty("antall") @Embedded val partyQuantity: PartyQuantity
)

@Access(AccessType.FIELD)
@Embeddable
data class PartyQuantity(
        @Column @JsonProperty("total") val total: Int?,
        @Column @JsonProperty("fhs") val early: Int?
)

@Embeddable
@Access(AccessType.FIELD)
data class Mandates(
        @Column @JsonProperty("resultat") val mandateResult: MandateResult
)

@Embeddable
@Access(AccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
data class MandateResult(
        @Column @JsonProperty("antall") val quantity: Int?,
        @Column @JsonProperty("endring") val mandateChange: Int?
        //@JsonProperty("utjevningAntall") val levelingQuantity: Long

)

@Embeddable
//Translate field name
data class Count(
        @Column @JsonProperty("forelopigFhs") val forelopigFhs: Double?,
        @Column @JsonProperty("forelopigVts") val forelopigVts: Double?,
        @Column @JsonProperty("forelopig") val forelopig: Double?,
        @Column @JsonProperty("endelig") val endelig: Double?,
        @Column @JsonProperty("oppgjor") val oppgjor: Double?
)

@Embeddable
data class Participation(
        @Column @JsonProperty("prosent") val participationPercent: Double?,
        @Column @JsonProperty("endring") val participationChange: Double?,
        @Column @JsonProperty("prosentStemmegiving") val participationPercentVoting: Double?
)

@Embeddable
data class Votes(
        @Column @JsonProperty("totalt") val total: Int?,
        @Column @JsonProperty("fhs") val earlyVotes: Int?,
        @Column @JsonProperty("totalForkastede") val votesTotalRejected: Int?,
        @Column @JsonProperty("fhsForkastede") val votesEarlyVotesRejected: Int?
)

@Embeddable
data class Voting(
        @Column @JsonProperty("totalGodkjente") val totalAccepted: Int?,
        @Column @JsonProperty("fhsGodkjente") val earlyVoteAccepted: Int?,
        @Column @JsonProperty("totalForkastede") val totalRejected: Int?,
        @Column @JsonProperty("fhsForkastede") val earlyVoteRejected: Int?

)

