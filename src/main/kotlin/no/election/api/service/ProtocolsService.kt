package no.election.api.service

import no.election.api.Utils
import no.election.api.model.Protocol
import no.election.api.repository.ProtocolRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProtocolsService {

    @Autowired
    lateinit var protocolRepository: ProtocolRepository

    fun get(): Protocol = protocolRepository.findAll().toList().first()

    fun getProtocol(municipality: String, county: String): Protocol {
        return protocolRepository.findByMunicipalityAndCounty(municipality = municipality, county = county).orElseThrow { Utils.notFoundProblem() }
    }

    fun getProtocol(nr: String): Protocol {
        return protocolRepository.findByEntityNr(nr).orElseThrow { Utils.notFoundProblem() }
    }
}