package no.election.api.service

import no.election.api.Utils.Companion.notFoundProblem
import no.election.api.Utils.Companion.toElectionEntityList
import no.election.api.dto.Parties
import no.election.api.repository.ElectionRepository
import no.election.api.repository.PartyIdRepository
import no.election.api.repository.PartyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class ElectionService {

    val topLevel: List<String> = listOf("land", "fylke", "kommune")

    @Autowired
    lateinit var electionRepository: ElectionRepository

    @Autowired
    lateinit var partyIdRepository: PartyIdRepository

    @Autowired
    lateinit var partyRepository: PartyRepository


    fun findByMunicipalityName(name: String) = electionRepository.findByNameAndLevel(name)


    fun party(code: String): List<Parties> {
        val partyId = partyIdRepository.findAllByCodeEquals(code)
        val partiesList = mutableListOf<Parties>()
        partyId.forEach { partyIdentifier ->
            partyRepository.findAllByPartyIdentifier(partyIdentifier)
                    .filter { it.partyIdentifier.code == code }
                    .forEach { partiesList.add(it) }
        }
        return partiesList

    }

    fun getAllMunicipalities() = electionRepository.findAllByLevel("kommune").toElectionEntityList()

    fun getAllMunicipalitiesByCounty(county: String) = electionRepository.findAllByParentAndLevel(level = "kommune", parent = county).toElectionEntityList()

    fun getResultByCounty(county: String, pageable: Pageable) = electionRepository.findByLevelAndParentContaining(parent = county, level = "kommune", pageable = pageable)

    fun getAllCounties() = electionRepository.findAllByParentAndLevel(parent = "norge", level = "fylke").toElectionEntityList()

    fun getByNr(nr: String) = electionRepository.findByNr(nr).orElseThrow { notFoundProblem() }


}



