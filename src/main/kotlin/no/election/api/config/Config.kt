package no.election.api.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.web.servlet.invoke
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.zalando.problem.ProblemModule
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.ext.ContextResolver
import javax.ws.rs.ext.Provider


class CustomBeanPostProcessor : BeanPostProcessor {
    private val logger = LoggerFactory.getLogger(javaClass)
    override fun postProcessAfterInitialization(bean: Any, beanName: String): Any? {
        if (beanName == "jacksonObjectMapper") {
            logger.info("Calling bean post processor after init for bean:: " + beanName + " class name: " + bean.javaClass)
            (bean as ObjectMapper).registerModule(ProblemModule())
        }
        return bean
    }
}

@Configuration
class Config {

    @Bean
    fun customBeanPostProcessor(): CustomBeanPostProcessor? {
        return CustomBeanPostProcessor()
    }

    @Bean
    fun customOpenAPI(@Value("\${springdoc.version:1}") appVersion: String?): OpenAPI? {
        return OpenAPI()
                .info(Info()
                        .title("Election API")
                        .version(appVersion)
                        .description("Early version of election api. Stay tuned")
                        .termsOfService("http://swagger.io/terms/")
                        .license(License().name("Apache 2.0").url("http://springdoc.org")))
    }

}

@EnableWebSecurity
class WebSecurity : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity?) {
        http {
            csrf { disable() }
            authorizeRequests {
                authorize("/**", permitAll)
            }
            cors { }
            headers { frameOptions { disable() } }
        }
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource? {
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", CorsConfiguration().applyPermitDefaultValues())
        return source
    }
}

@Provider
@Produces(APPLICATION_JSON)
class ObjectMapperProvider : ContextResolver<ObjectMapper> {

    private val objectMapper = objectMapper()

    override fun getContext(objectType: Class<*>): ObjectMapper {
        return objectMapper
    }

    private fun objectMapper(): ObjectMapper {
        val mapper = ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        return mapper
    }

}