package no.election.api

import no.election.api.dto.ElectionEntity
import no.election.api.model.Result
import org.apache.commons.lang3.StringUtils
import org.zalando.problem.Problem
import org.zalando.problem.Status

class Utils {
    companion object {
        fun notFoundProblem() =
                Problem.valueOf(Status.NOT_FOUND)

        fun List<Result>.toElectionEntityList() = this.map {
            ElectionEntity(
                    name = StringUtils.stripAccents(it.name),
                    county = it.parent!!,
                    nr = it.nr,
                    originalName = it.originalName,
                    parentOriginalName = it.parentOriginalName!!
            )
        }
    }
}