package no.election.api.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import no.election.api.Problem
import no.election.api.model.Protocol
import no.election.api.model.Result
import no.election.api.service.ElectionService
import no.election.api.service.ProtocolsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController()
@RequestMapping("api")
class Controller {
    @Autowired
    lateinit var electionService: ElectionService

    @Autowired
    lateinit var protocolsService: ProtocolsService

    @Operation(
            parameters = [
                Parameter(name = "county", examples = [
                    ExampleObject(name = "Rogaland", value = "rogaland"),
                    ExampleObject(name = "Innlandet", value = "innlandet")
                ]),
                Parameter(name = "municipality", examples = [
                    ExampleObject(name = "Klepp", value = "klepp"),
                    ExampleObject(name = "Gjøvik", value = "gjovik")
                ])
            ],
            responses = [
                ApiResponse(responseCode = "200", content = [Content(mediaType = "application/json", schema = Schema(implementation = Protocol::class))]),
                ApiResponse(responseCode = "404", content = [Content(mediaType = "application/problem+json", schema = Schema(implementation = Problem::class))])
            ])
    @GetMapping("/protocol/{county}/{municipality}")
    fun getProtocol(
            @PathVariable county: String,
            @PathVariable municipality: String
    ): ResponseEntity<Protocol> {
        return ResponseEntity.ok(protocolsService.getProtocol(municipality = municipality, county = county))
    }

    @Operation(
            parameters = [
                Parameter(name = "nr", examples = [
                    ExampleObject(name = "Eigersund", value = "1101")
                ])
            ],
            responses = [
                ApiResponse(responseCode = "200", content = [Content(mediaType = "application/json", schema = Schema(implementation = Protocol::class))]),
                ApiResponse(responseCode = "404", content = [Content(mediaType = "application/problem+json", schema = Schema(implementation = Problem::class))])
            ])
    @GetMapping("/protocol/{nr}")
    fun getProtocol(
            @PathVariable nr: String
    ): ResponseEntity<Protocol> {
        return ResponseEntity.ok(protocolsService.getProtocol(nr = nr))
    }

    @GetMapping("/results")
    fun getResultsByCounty(pageable: Pageable, county: String): ResponseEntity<Page<Result>> {
        return electionService.getResultByCounty(county, pageable).toResponseEntity()
    }

    @GetMapping("/results/{nr}")
    fun getResult(@PathVariable nr: String) = ResponseEntity.ok(electionService.getByNr(nr))

    @GetMapping("/municipalities")
    fun listMunicipalities() = ResponseEntity.ok(
            ResponseHolder(electionService.getAllMunicipalities())
    )

    @GetMapping("/municipalities/{name}")
    fun municipality(@PathVariable name: String): ResponseEntity<List<Result>> {
        return ResponseEntity.ok(electionService.findByMunicipalityName(name))
    }

    @GetMapping("/counties")
    fun listCounties() = ResponseHolder(electionService.getAllCounties())

    @GetMapping("/counties/{county}/municipalities")
    fun listMunicipalitiesByCounty(@PathVariable county: String) = ResponseEntity.ok(ResponseHolder(electionService.getAllMunicipalitiesByCounty(county)))

    @GetMapping("/parties/{code}")
    fun party(@PathVariable code: String) = ResponseEntity.ok(electionService.party(code))
}

private fun <T> T.toResponseEntity() = ResponseEntity.ok(this)

data class ResponseHolder<T>(val items: List<T>)