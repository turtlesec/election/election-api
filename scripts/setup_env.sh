#!/bin/sh
echo SPRING_PROFILES_ACTIVE=prod >> .env
echo jdbc_connection_string=${jdbc_connection_string} >> .env
echo db_username=${db_username} >> .env
echo db_password=${db_password} >> .env
echo TAG=${CI_COMMIT_SHA} >> .env